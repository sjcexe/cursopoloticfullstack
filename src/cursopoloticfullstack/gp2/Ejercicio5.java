/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio5 {
    public static void main(String[] args) {
        //Escribir un algoritmo que lea un valor n por teclado y muestre los
        //primeros n términos de la sucesión de Fibonacci.
        //Tip: la sucesión de Fibonacci comienza con 0, luego 1 y a partir de allí
        //cada nuevo número es la suma de los dos anteriores.
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de números de la secuencia Fibonacci que quiere ver: ");
        int n = s.nextInt();
        int[] array = new int[n];
        fibonacci(array);
        System.out.println("Estos son los números: " + Arrays.toString(array));
    }

    public static void fibonacci(int[] array){
        array[0]= 0;
        array[1]= 1;
        for (int i = 2; i < array.length; i++){
            array[i] = array[i - 1] + array[i - 2];
        }
    }
}
