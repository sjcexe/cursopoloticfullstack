/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio7 {
    public static void main(String[] args) {
        //Del listado de abajo, determinar qué animal eligió el usuario mediante
        //la realización de tres preguntas del tipo SI/NO acerca de las tres
        //características elegidas (herbívoro, mamífero, doméstico). Mostrar el
        //resultado por pantalla.
        //Animal Herbívoro Mamífero Doméstico
        //Alce SI SI NO
        //Caballo SI SI SI
        //Caracol SI NO NO
        //Cóndor NO NO NO
        //Gato NO SI SI
        //León NO SI NO
        //Pitón NO NO SI
        //Tortuga SI NO SI
        Scanner scanner = new Scanner(System.in);

        System.out.println("¡Adivina qué animal elegiste!");

        System.out.print("¿El animal es herbívoro? (SI/NO): ");
        String respuestaHerbivoro = scanner.nextLine().toUpperCase();

        System.out.print("¿El animal es mamífero? (SI/NO): ");
        String respuestaMamifero = scanner.nextLine().toUpperCase();

        System.out.print("¿El animal es doméstico? (SI/NO): ");
        String respuestaDomestico = scanner.nextLine().toUpperCase();

        if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("NO")) {
            System.out.println("El animal elegido es el Alce.");
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("SI")) {
            System.out.println("El animal elegido es el Caballo.");
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("NO")) {
            System.out.println("El animal elegido es el Caracol.");
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("NO")) {
            System.out.println("El animal elegido es el Cóndor.");
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("SI")) {
            System.out.println("El animal elegido es el Gato.");
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("SI") && respuestaDomestico.equals("NO")) {
            System.out.println("El animal elegido es el León.");
        } else if (respuestaHerbivoro.equals("NO") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("SI")) {
            System.out.println("El animal elegido es la Pitón.");
        } else if (respuestaHerbivoro.equals("SI") && respuestaMamifero.equals("NO") && respuestaDomestico.equals("SI")) {
            System.out.println("El animal elegido es la Tortuga.");
        } else {
            System.out.println("No se pudo determinar el animal elegido.");
        }
    }
}
