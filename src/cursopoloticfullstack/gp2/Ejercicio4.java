/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio4 {
    public static void main(String[] args) {
        //Escribir un algoritmo que calcule el factorial de un número ingresado
        //por teclado.
        //Tip: el factorial de un número n es el resultado de multiplicar todos
        //los números enteros desde 1 hasta n.
        //Por ejemplo, el factorial de 5 es 1 × 2 × 3 × 4 × 5 = 120.
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un numero entero: ");
        int numero = s.nextInt();
        System.out.println("El factorial de " + numero + " es: " + factorial(numero));
    }

    public static int factorial(int n){
        int factorial = 1;
        for (int i = 1; i <= n; i++){
            factorial *= i;
        }
        return factorial;
    }
}
