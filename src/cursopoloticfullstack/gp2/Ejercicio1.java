/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio1 {
    public static void main(String[] args) {
        //Escribir un programa que reciba un número entero por teclado. A
        //continuación, mostrar la tabla de multiplicar de ese número.
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un número entero: ");
        int numero = s.nextInt();
        System.out.println("La tabla de multiplicar es: ");
        for (int i = 1; i <= 10; i++) {
            System.out.println(numero + " x " + i + " = " + (numero * i));
        }
    }
}
