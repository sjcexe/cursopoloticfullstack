/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio2 {
    public static void main(String[] args) {
        //Escribir un programa que lea una palabra por teclado y determine si
        //es un palíndromo.
        //Tip: los palíndromos son palabras que se leen igual de izquierda a
        //derecha y viceversa. Por ejemplo, reconocer.
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese una palabra, veremos si es un palíndromo: ");
        String palabra = s.nextLine();

        if (esPalindromo(palabra)){
            System.out.println("Es un palíndromo.");
        } else {
            System.out.println("No es un palíndromo.");
        }

    }
    public static boolean esPalindromo(String palabra){
        String palabraInvertida = "";

        for (int i = palabra.length() -1; i >=0 ; i--){
            palabraInvertida += palabra.charAt(i);
        }

        if (palabra.equalsIgnoreCase(palabraInvertida)){
            return true;
        } else{
            return false;
        }
    }
}
