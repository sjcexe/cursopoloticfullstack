/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio3 {
    public static void main(String[] args) {
        //Escribir un programa que ordene un arreglo de números enteros de
        //manera ascendente.
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese 5 numeros enteros para ordenarlos:");

        int[] arreglo = new int[5];
        for (int i = 0; i < 5; i++){
            arreglo[i] = s.nextInt();
        }
        System.out.println("Arreglo original: " + Arrays.toString(arreglo));
        ordenAscendente(arreglo);
        System.out.println("Arreglo original: " + Arrays.toString(arreglo));

    }

    public static void ordenAscendente(int[] arreglo){
        int n = arreglo.length;
        for (int i = 0; i < n - 1; i++){
            for (int j = 0; j < n - 1; j++){
                if (arreglo[j] > arreglo[j + 1]){
                    //vamos ordenando los elementos
                    int temp = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = temp;
                }
            }
        }
    }
}
