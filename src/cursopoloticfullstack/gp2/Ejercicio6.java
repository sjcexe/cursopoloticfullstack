/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Ejercicio6 {
    public static void main(String[] args) {
        //Escribir un algoritmo que imprima el listado de los números primos
        //menores que 200. Aclaración: el número 1 no es primo.
        //Tip: un número es primo si es divisible únicamente por 1 y por sí
        //mismo.
        //Por ejemplo, el 7 es primo porque sólo es divisible por 1 y por 7.
        //El 6 no es primo porque es divisible por 1, por 2, por 3 y por 6.
        System.out.println("Listado de números primos menores que 200:");

        for (int i = 2; i < 200; i++) {
            if (esPrimo(i)) {
                System.out.print(i + " ");
            }
        }
    }
    public static boolean esPrimo(int numero) {
        // esta comprobacion sirve para valores menores a 1 o valores negativos (no necesaria para este ejercicio)
        if (numero <= 1) {
            return false;
        }
        // mediante Math.sqrt() obtenemos la raiz cuadrada del valor, y comprobamos unicamente los valores menores
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }

        return true;
    }
}
