/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package cursopoloticfullstack;

import cursopoloticfullstack.gp3.Libro;

/**
 *
 * @author sjcex
 */
public class Main {
    
    public static void main(String[] args) {
        Libro libro = new Libro();

        libro.cargarLibro();
        libro.informarDatos();
    }
    
}
