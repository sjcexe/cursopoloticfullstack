/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp3;

/**
 *
 * @author sjcex
 */
public class Circunferencia {
    private double radio;

    public Circunferencia(float radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }

    public double area(Circunferencia circunferencia) {
        return Math.PI * Math.pow(circunferencia.getRadio(), 2);
    }

    public double perimetro(Circunferencia circunferencia) {
        return 2 * Math.PI * circunferencia.getRadio();
    }
}
