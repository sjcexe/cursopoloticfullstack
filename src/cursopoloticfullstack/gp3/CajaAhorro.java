/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp3;

/**
 *
 * @author sjcex
 */
public class CajaAhorro extends CuentaBancaria {
    // Constructor por defecto
    public CajaAhorro() {
    }

    // Constructor con DNI, saldo y número de cuenta
    public CajaAhorro(long dniCliente, double saldoActual, int numero) {
        super(dniCliente, saldoActual, numero);
    }

    // Método retirar
    public double retirar(double retiro) {
        double saldoActual = getSaldoActual();
        if (retiro > 0) {
            if (retiro <= saldoActual) {
                saldoActual -= retiro;
                setSaldoActual(saldoActual);
                return retiro;
            } else {
                setSaldoActual(0);
                return saldoActual;
            }
        }
        return 0;
    }
}
