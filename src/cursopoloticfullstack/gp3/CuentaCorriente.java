/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp3;

/**
 *
 * @author sjcex
 */
public class CuentaCorriente extends CuentaBancaria {
    // Constructor por defecto
    public CuentaCorriente() {
    }

    // Constructor con DNI, saldo y número de cuenta
    public CuentaCorriente(long dniCliente, double saldoActual, int numero) {
        super(dniCliente, saldoActual, numero);
    }

    // Método retirar
    public double retirar(double retiro) {
        double saldoActual = getSaldoActual();
        if (retiro > 0) {
            saldoActual -= retiro;
            setSaldoActual(saldoActual);
            return retiro;
        }
        return 0;
    }
}
