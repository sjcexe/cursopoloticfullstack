/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp3;

/**
 *
 * @author sjcex
 */
public class CuentaBancaria {
    private int numero;
    private long dniCliente;
    private double saldoActual;

    // Constructor por defecto
    public CuentaBancaria() {
    }

    // Constructor con DNI, saldo y número de cuenta
    public CuentaBancaria(long dniCliente, double saldoActual, int numero) {
        this.dniCliente = dniCliente;
        this.saldoActual = saldoActual;
        this.numero = numero;
    }

    // Métodos getters y setters
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public long getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(long dniCliente) {
        this.dniCliente = dniCliente;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    // Método ingresar
    public void ingresar(double ingreso) {
        if (ingreso > 0) {
            saldoActual += ingreso;
        }
    }

    // Método extraccionRapida
    public double extraccionRapida() {
        double importeRetirado = saldoActual * 0.2;
        saldoActual -= importeRetirado;
        return importeRetirado;
    }

    // Método consultarSaldo
    public double consultarSaldo() {
        return saldoActual;
    }

    // Método consultarDatos
    public void consultarDatos() {
        System.out.println("Número de cuenta: " + numero);
        System.out.println("DNI del cliente: " + dniCliente);
        System.out.println("Saldo actual: " + saldoActual);
    }
}
