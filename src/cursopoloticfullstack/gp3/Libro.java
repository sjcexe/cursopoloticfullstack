/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cursopoloticfullstack.gp3;

import java.util.Scanner;

/**
 *
 * @author sjcex
 */
public class Libro {
    
    private String isbn;
    private String titulo;
    private String autor;
    private int paginas;

    public Libro(String isbn, String titulo, String autor, int paginas) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
    }

    public Libro() {
    }

    public void cargarLibro() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el número de ISBN: ");
        isbn = scanner.nextLine();

        System.out.print("Ingrese el título del libro: ");
        titulo = scanner.nextLine();

        System.out.print("Ingrese el autor del libro: ");
        autor = scanner.nextLine();

        System.out.print("Ingrese el número de páginas: ");
        paginas = scanner.nextInt();
    }

    public void informarDatos() {
        System.out.println("Número de ISBN: " + isbn);
        System.out.println("Título del libro: " + titulo);
        System.out.println("Autor del libro: " + autor);
        System.out.println("Número de páginas: " + paginas);
    }
}
